var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.RXPlayer = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.getPosition()
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    return this.player.getPlaybackRate()
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    return this.tag.webkitDroppedFrameCount
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.player.getVideoDuration()
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return this.player.getVideoBitrate()
  },

  /** Override to return rendition */
  getRendition: function () {
    var bitrate = this.player.getVideoBitrate()
    var width = null
    var height = null
    try {
      var rep = this.player.getCurrentRepresentations()
      if (rep && rep.video) {
        width = rep.video.width
        height = rep.video.height
      }
    } catch (e) {
      // nothing
    }
    return youbora.Util.buildRenditionString(width, height, bitrate)
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.player.isLive()
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.getUrl()
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.player.version
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Rx-player'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    this.monitorPlayhead(true, false)
    // References
    this.references = {
      playerStateChange: this.stateChangeListener.bind(this),
      warning: this.warningListener.bind(this),
      error: this.errorListener.bind(this),
      positionUpdate: this.positionUpdateListener.bind(this)
    }

    // Register listeners
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }

    try {
      this.tag = this.player.getVideoElement()
    } catch (e) {
      this.tag = this.player
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    if (this.monitor) this.monitor.stop()
    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },
  /** Listener for 'positionUpdate' event. */
  positionUpdateListener: function (e) {
    if (e.position > 0.2) {
      this.fireJoin()
      this.fireStart()
    }
  },

  /** Listener for 'warning' event. */
  warningListener: function (e) {
    this.fireError()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    this.fireError()
    this.fireStop()
  },

  /** Listener for 'playerStateChange' event. */
  stateChangeListener: function (e) {
    youbora.Log.debug(e)
    switch (e) {
      // case 'LOADED':
      case 'LOADING':
        if (this.player.videoElement.autoplay) {
          this.fireStart()
        }
        break
      case 'BUFFERING':
        // this.fireBufferBegin()
        break
      case 'SEEKING':
        this.fireSeekBegin()
        break
      case 'PLAYING':
        this.fireStart()
        this.fireResume()
        // this.fireBufferEnd()
        this.fireSeekEnd()
        break
      case 'PAUSED':
        this.firePause()
        break
      case 'ENDED':
      case 'STOPPED':
        setTimeout(function () {
          this.fireStop()
        }.bind(this), 200)
    }
  }
})

module.exports = youbora.adapters.RXPlayer
