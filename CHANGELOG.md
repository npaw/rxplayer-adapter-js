## [6.7.0] - 2020-05-04
### Added
- Dropped frames and improved rendition
### Library
- Packaged with `lib 6.7.6`

## [6.4.0] - 2018-05-28
### Library
- Packaged with `lib 6.4.30`

## [6.3.0] - 2018-08-08
### Library
- Packaged with `lib 6.3.5`
